/* eslint-disable no-console */
"use strict";
const { useState } = React;


function App() {
  const [currentSearch, setSearch] = useState("");
  const [resultList, setResultList] = useState([]);
  const [displayImage, setDisplayImage] = useState("");
  const [baseUrl, setBaseUrl] = useState("");


  return (
    <div className="wrapper">
      <header>
        <h1>Art Institute of Chicago Search</h1>
        <form onSubmit={searchRequest}>
          <input value={currentSearch} onInput={ (e) => {setSearch(e.target.value)}}/>
          <button type="submit">search</button>
        </form>
        <ul>
          {resultList.map( (result) => <li key={result.id} onClick={() => selectHandler(result)}> {result.title} </li> ) }
        </ul>
        <div>Aidan Holmes</div>
      </header>
      <main >
        <h2>{displayImage.title}</h2>
        <div className="artist-info">{displayImage.artist_display}</div>
        <Image image_id={displayImage.image_id}/>
        <Description />
        <dl>
          <DescriptiveItem title="Medium">{displayImage.medium_display}</DescriptiveItem>
          <DescriptiveItem title="Dimensions">{displayImage.dimensions}</DescriptiveItem>
          <DescriptiveItem title="Credit Line">{displayImage.credit_line}</DescriptiveItem>
        </dl>
      </main>
    </div>
  );

  function searchRequest(e){
    e.preventDefault();
    console.log({currentSearch});
    request({currentSearch})
  };

  function dangerHTML(){
    return {__html: displayImage.description};
  }
  function Description(){
    return <div dangerouslySetInnerHTML={dangerHTML()}></div>
  }

  function Image({ image_id }){
    if(baseUrl){
      return <img crossOrigin="anonymous" src={`${baseUrl}/${image_id}/full/843,/0/default.jpg`}></img>
    }
    else{
      return null;
    }
  };

  function DescriptiveItem({ children, title }){
    if(children){
      return(
        <div>
          <dt>{title}</dt>
          <dd>{children}</dd>
        </div>
      )
    }
    return null;
  }

  function selectHandler(e){
    requestArt(e.id);
  }

  function request({currentSearch}){
    return fetch(`https://api.artic.edu/api/v1/artworks/search?q=${currentSearch}`, {})
        .then( response => { 
            //console.log( "1) Response object:", response);
            
            if( !response.ok ) { 
                throw new Error("Not 2xx response", {cause: response});
            }
            
            return response.json();
        })

        .then( obj => {
            console.log(obj);
            console.log(obj.data);
            setResultList(obj.data);
        })

        .catch( err => {
            console.error("3)Error:", err);
        });
  }

  function requestArt(id){
    return fetch(`https://api.artic.edu/api/v1/artworks/${id}?fields=id,title,image_id,credit_line,artist_display,description,medium_display,dimensions`, {})
        .then( response => { 

            if( !response.ok ) { 
                throw new Error("Not 2xx response", {cause: response});
            }
            
            return response.json();
        })

        .then( obj => {
            setDisplayImage(obj.data);
            setBaseUrl(obj.config.iiif_url);
        })

        .catch( err => {
            console.error("3)Error:", err);
        });
  }

}


ReactDOM.render(
  <App />,
  document.getElementById("react-root")
);
